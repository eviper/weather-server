<?php

namespace Database\Seeders;


use App\Models\History;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class HistorySeeder extends Seeder
{
    private int   $days    = 6 * 30;

    private array $history = [];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($subDay = 0; $subDay < $this->days; $subDay++) {
            $this->history[] = History::factory()->raw([
                'date_at' => Carbon::now()->subDays($subDay)->format('Y-m-d'),
            ]);
        }

        History::query()->insert($this->history);
    }
}

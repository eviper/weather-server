<?php

namespace Database\Factories;


use App\Models\History;
use Illuminate\Database\Eloquent\Factories\Factory;

class HistoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     */
    protected $model = History::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        return [
            'temp'    => $this->faker->randomFloat(1, -50, 100),
            'date_at' => fn() => $this->faker->date(),
        ];
    }
}

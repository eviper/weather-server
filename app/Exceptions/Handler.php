<?php

namespace App\Exceptions;


use App\Http\JsonRpcResponse;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [];

    /**
     * A list of the internal exception types that should not be reported.
     *
     * @var string[]
     */
    protected $internalDontReport = [];

    /**
     * @param Throwable $e
     *
     * @return Throwable
     */
    protected function prepareException(Throwable $e): Throwable
    {
        if ($e instanceof ValidationException) {
            $e = new JsonRpcException($e->validator->errors()->first());
        } else {
            $e = new JsonRpcException($e->getMessage());
        }

        return $e;
    }

    public function register()
    {
        $this->renderable(function (JsonRpcException $e) {
            return JsonRpcResponse::error($e->getParams(), $e->getCode());
        });
    }
}

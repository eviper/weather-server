<?php

namespace App\Exceptions;


use Exception;
use Throwable;

class JsonRpcException extends Exception
{
    private $params;

    public function __construct($params, $message = "", $code = 0, Throwable $previous = null) {
        $this->params = $params;

        parent::__construct($message, $code, $previous);
    }

    public function getParams()
    {
        return $this->params;
    }
}

<?php

namespace App\Http\Middleware;


use App\Http\JsonRpcResponse;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;

class JsonRpcMiddleware
{
    private $action;

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $this->validation($request);

        $request->route()->uses(implode('@', $this->action));

        return $next($request);
    }

    private function validation(Request $request)
    {
        Validator::extend('json_version', static function (string $a, string $v) {
            return $v === JsonRpcResponse::JSON_RPC_VERSION;
        });
        Validator::extend('valid_method', function () use ($request) {
            $action = $this->getAction($request);

            return $action && isset($action[1]);
        });

        Validator::validate($request->json()->all(), [
            'jsonrpc' => 'required|string|json_version',
            'method'  => 'required|string|min:3|valid_method',
            'params'  => 'required|array',
            'id'      => 'required|integer',
        ]);
    }

    private function getAction($request)
    {
        if (!isset($this->action)) {
            $this->action = explode('.', $request->get('method'));
            if ($this->action) {
                $this->action[0] = 'App\\Http\\Controllers\\' . Str::ucfirst(Str::camel($this->action[0])) . 'Controller';
            }
        }

        return $this->action;
    }
}

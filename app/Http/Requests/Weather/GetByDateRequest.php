<?php

namespace App\Http\Requests\Weather;


use Illuminate\Foundation\Http\FormRequest;

class GetByDateRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'params.date' => 'required|date_format:Y-m-d',
        ];
    }
}

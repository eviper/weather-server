<?php

namespace App\Http\Requests\Weather;


use Illuminate\Foundation\Http\FormRequest;

class GetHistoryRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'params.lastDays' => 'required|integer|min:1|max:180',
        ];
    }
}

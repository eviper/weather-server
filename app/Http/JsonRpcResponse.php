<?php

namespace App\Http;


use Illuminate\Http\JsonResponse;

class JsonRpcResponse
{
    public const JSON_RPC_VERSION = '2.0';

    public static function success($result): JsonResponse
    {
        return response()->json([
            'jsonrpc' => self::JSON_RPC_VERSION,
            'result'  => $result,
            'id'      => request()->get('id'),
        ]);
    }

    public static function error($error, $code): JsonResponse
    {
        return response()->json([
            'jsonrpc' => self::JSON_RPC_VERSION,
            'error'  => [
                'code' => $code,
                'message' => $error,
            ],
            'id'      => null,
        ]);
    }
}

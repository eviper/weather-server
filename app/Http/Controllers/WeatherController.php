<?php

namespace App\Http\Controllers;



use App\Http\JsonRpcResponse;
use App\Http\Requests\Weather\GetByDateRequest;
use App\Http\Requests\Weather\GetHistoryRequest;
use App\Resources\WeatherRepository;
use Illuminate\Http\JsonResponse;

class WeatherController extends Controller
{
    public function getByDate(GetByDateRequest $request, WeatherRepository $repository): JsonResponse
    {
        $date = $request->json('params.date');

        $response = $repository->getByDate($date);

        return JsonRpcResponse::success($response);
    }

    public function getHistory(GetHistoryRequest $request, WeatherRepository $repository): JsonResponse
    {
        $date = $request->json('params.lastDays');

        $response = $repository->getHistory($date);

        return JsonRpcResponse::success($response);
    }
}

<?php

namespace App\Resources;


use App\Models\History;

class WeatherRepository
{
    public function getByDate(string $date)
    {
        $history = History::query()
            ->where('date_at', $date)
            ->first(['temp']);

        if (!$history) {
            throw new \Exception('History by date ' . $date . ' not found');
        }

        return $history->temp;
    }

    public function getHistory(int $lastDays): array
    {
        return History::query()
            ->orderByDesc('date_at')
            ->limit($lastDays)
            ->get(['temp', 'date_at'])
            ->mapWithKeys(static fn(History $h) => [$h->date_at => $h->temp])
            ->toArray();
    }
}
